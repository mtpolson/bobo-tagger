/**
 * A jQuery plugin boilerplate.
 * Author: Jonathan Nicol @f6design
 */
;(function($) {
 
	function boboShowTags(element, options) {
		var imageTagger = element;
		//var $el = $(element);
	
		//options = $.extend({}, $.fn.boboShowTags.defaults, options);
		
		function init() {
			// Add any initialization logic here...
				
			//imageTagger = this;
			
			// default options
			imageTagger.options = $.extend({}, $.fn.boboShowTags.defaults, options);
			//$.extend(imageTagger.options, options);
			
			var $t = $(imageTagger);
			$t.wrap('<span></span>');
			imageTagger.wrapper = $t.parent();
			
			imageTagger.wrapper.css(imageTagger.options.wrapperCss);
			
			// ! add tags if already set
			if (imageTagger.options.data.length > 0) {
				for(var i in imageTagger.options.data) {
					imageTagger.options.data[i].id = "tagger_" + i;
					// create element
					var $tag = $('<div class="tagger-wrapper" id="tagger_' + i + '"></div>').appendTo(imageTagger.wrapper).css(imageTagger.options.tagCss).css({"position" : "absolute", "top": (imageTagger.options.data[i].topLeft.y + imageTagger.options.data[i].bottomRight.y) / 2, "left": (imageTagger.options.data[i].bottomRight.x + imageTagger.options.data[i].topLeft.x) / 2}).width(0).height(0);
					$tag.html('<span class="tagger-dot"></span><div class="tagger-tag" id="tagger_' + i + '"></div>');
					
					$tag.find(".tagger-tag").css({"top": (imageTagger.options.data[i].bottomRight.y - imageTagger.options.data[i].topLeft.y) / 2 * -1, "left": (imageTagger.options.data[i].bottomRight.x - imageTagger.options.data[i].topLeft.x) / 2 * -1 }).width(imageTagger.options.data[i].bottomRight.x - imageTagger.options.data[i].topLeft.x).height(imageTagger.options.data[i].bottomRight.y - imageTagger.options.data[i].topLeft.y).hide();
					
					$tag.find(".tagger-dot").click(function(e) {
						e.stopPropagation();
						var $t = $(this);
						var $p = $t.parents('.tagger-wrapper');
						imageTagger.wrapper.find("." + imageTagger.options.infoClass).find(".tagger-close").click();
						$p.css("z-index", "51").find("." + imageTagger.options.infoClass).show();
						$p.find(".tagger-tag").show();
					});
					
					
					imageTagger.options.data[i].el = $tag;
					
					if (imageTagger.options.infoTemplate != '') {
						imageTagger.options.data[i].el.append('<div class="' + imageTagger.options.infoClass + '" data-index="' + i + '">' + imageTagger.options.infoTemplate + '</div>');
						// prepopulate data if available
						if (typeof imageTagger.options.data[i].form_data != "undefined") {
							for(var j in imageTagger.options.data[i].form_data) {
								if (j == "urls") {
									for (var k in imageTagger.options.data[i].form_data[j]) {
										imageTagger.options.data[i].el.find("." + k).attr("href", imageTagger.options.data[i].form_data[j][k]);
									}
								}
								else if (j == "images") {
									for (var k in imageTagger.options.data[i].form_data[j]) {
										imageTagger.options.data[i].el.find("." + k).attr("src", imageTagger.options.data[i].form_data[j][k]);
									}
								}
								else {
									imageTagger.options.data[i].el.find("." + j).text(imageTagger.options.data[i].form_data[j]);
								}
							}
							imageTagger.options.data[i].el.find("." + imageTagger.options.infoClass).hide();
						}
						// setup options
						imageTagger.options.data[i].el.find("." + imageTagger.options.infoClass)
							.css({"position":"absolute", "left": (imageTagger.options.data[i].bottomRight.x - imageTagger.options.data[i].topLeft.x) / 2 + 10, "top": Math.round((imageTagger.options.data[i].bottomRight.y - imageTagger.options.data[i].topLeft.y) / 2 * -1) })
							.find('.tagger-close').click(function(e) {
								$(this).parents('.' + imageTagger.options.infoClass).hide().parents('.tagger-wrapper').css("z-index", "5").find(".tagger-tag").hide();
								e.stopPropagation();
							});
					}
					
				}
			}
			
		}
		
		function open(idx) {
			imageTagger.options.data[idx].el.find(".tagger-dot").click();
		}
		
		function option (key, val) {
			if (val) {
				options[key] = val;
			} else {
				return options[key];
			}
		}
	
		init();
	
		return {
			option: option,
			open: open
		};
	}
	
	$.fn.boboShowTags = function(options) {
		if (typeof arguments[0] === 'string') {
			var methodName = arguments[0];
			var args = Array.prototype.slice.call(arguments, 1);
			var returnVal;
			this.each(function() {
				if ($.data(this, 'boboShowTags') && typeof $.data(this, 'boboShowTags')[methodName] === 'function') {
					returnVal = $.data(this, 'boboShowTags')[methodName].apply(this, args);
				} else {
					throw new Error('Method ' +  methodName + ' does not exist on jQuery.');
				}
			});
			if (returnVal !== undefined){
				return returnVal;
			} else {
				return this;
			}
		} else if (typeof options === "object" || !options) {
			return this.each(function() {
				if (!$.data(this, 'boboShowTags')) {
				  $.data(this, 'boboShowTags', new boboShowTags(this, options));
				}
			});
		}
	};
	
	$.fn.boboShowTags.defaults = {
		"infoTemplate" : "",
		"infoClass" : "tagger-info-panel",
		"data" : [], // existing tags
		"wrapperCss" : {
			"position": "relative",
			"display" : "inline-block",
			"padding": 0,
			"margin": 0
		},
		"tagCss" : {
			"position": "absolute",
			"box-sizing": "border-box",
			"padding": 0,
			"border": "solid 2px green",
			"margin": 0
		}
	};
 
})(jQuery);