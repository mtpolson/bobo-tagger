(function($, window) {

	$.fn.boboTagger = function(options) {
		
		///////////////// PUBLIC FUNCTIONS \\\\\\\\\\\\\\\\
		var methods = {
		
			init: function(options) {
			
			
				var defaults = {
					"minTagWidth" : 25,
					"minTagHeight" : 25,
					"infoTemplate" : "",
					"infoClass" : "tagger-info-panel",
					"save": null,
					"cancel": null,
					"delete": null,
					//"update": null,
					"isDrawing" : false,
					"data" : [], // existing tags
					"wrapperCss" : {
						"position": "relative",
						"display" : "inline-block",
						"padding": 0,
						"margin": 0
					},
					"tagCss" : {
						//"position": "absolute",
						"box-sizing": "border-box",
						"padding": 0,
						"border": "solid 2px green",
						"margin": 0
					}
				};
				
					
				imageTagger = this;
				
				// default options
				imageTagger.options = $.extend({}, defaults, options);
				
				var $t = $(imageTagger);
				$t.wrap('<span></span>');
				imageTagger.wrapper = $t.parent();
				
				imageTagger.wrapper.css(imageTagger.options.wrapperCss).mousedown(function(e) {
					e.preventDefault();
					// setup
					var $t = $(this);
					// hide all tags
					imageTagger.wrapper.find('.' + imageTagger.options.infoClass).hide();
					imageTagger.wrapper.find(".tagger-tag").hide();
					// add new data value
					var id = "tagger_" + imageTagger.options.data.length;
					var $tag = $('<div class="tagger-wrapper" style="position:absolute"></div>').appendTo($t);
					$tag.html('<span class="tagger-dot"></span><div class="tagger-tag" id="' + id + '"></div>');
					$tag.find(".tagger-tag").css(imageTagger .options.tagCss);
					var coords = coordinates($t, e.clientX, e.clientY);
					var tag = {
						"id" : id,
						"externalID" : null,
						"el" : $tag,
						"topLeft" : coords
					};
					imageTagger.options.data.push(tag);
					imageTagger.options.isDrawing = imageTagger.options.data.length - 1;
				}).mouseup(function(e) {
					e.preventDefault();
					var $t = $(this);
					if (typeof imageTagger.options.data[imageTagger.options.isDrawing].topLeft != "undefined" && typeof imageTagger.options.data[imageTagger.options.isDrawing].bottomRight != "undefined" ) {
						var tagCoords = {
							"topLeft" : imageTagger.options.data[imageTagger.options.isDrawing].topLeft,
							"bottomRight" : imageTagger.options.data[imageTagger.options.isDrawing].bottomRight
						};
					}
					else {
						var coords = coordinates($t, e.clientX, e.clientY);
						var tagCoords = tagCoordinates($t, imageTagger.options.data[imageTagger.options.isDrawing].topLeft, coords);
					}
					$.extend(imageTagger.options.data[imageTagger.options.isDrawing], tagCoords);
					// check to make sure this meets minimum requirements
					if (imageTagger.options.minTagWidth > tagCoords.bottomRight.x - tagCoords.topLeft.x || imageTagger.options.minTagHeight > tagCoords.bottomRight.y - tagCoords.topLeft.y) {
						// not big enough - remove and return
						imageTagger.options.data[imageTagger.options.isDrawing].el.remove();
						imageTagger.options.data.splice(imageTagger.options.isDrawing, 1);
						imageTagger.options.isDrawing = false;
						return;
					}
					imageTagger.options.data[imageTagger.options.isDrawing].el
						.css({"top": (tagCoords.topLeft.y + tagCoords.bottomRight.y) / 2, "left": (tagCoords.topLeft.x + tagCoords.bottomRight.x) / 2})
						.width(0)
						.height(0)
						.click(function(e) {
							e.stopPropagation();
							var $t = $(this);
							$("." + imageTagger.options.infoClass).hide();
							$(".tagger-tag").hide();
							$t.find(".tagger-tag").show();
							$t.find("." + imageTagger.options.infoClass).show();
						}).mousedown(function(e) {
							e.stopPropagation();
						})
						.mouseup(function(e) {
							e.stopPropagation();
						});
					// make tagger-tag match width & height of tagger-wrapper
					// hide all tags
					//$(".tagger-tag").not(imageTagger.options.data[imageTagger.options.isDrawing].el.find(".tagger-tag")).hide();
					// show current again
					imageTagger.options.data[imageTagger.options.isDrawing].el.find(".tagger-tag").width(tagCoords.bottomRight.x - tagCoords.topLeft.x).height(tagCoords.bottomRight.y - tagCoords.topLeft.y).css({"position" : "absolute", "left" : (tagCoords.bottomRight.x - tagCoords.topLeft.x) / 2 * -1, "top": (tagCoords.bottomRight.y - tagCoords.topLeft.y) / 2 * -1 })
						.mousedown(function(e) {
							e.stopPropagation();
						})
						.mouseup(function(e) {
							e.stopPropagation();
						}
					);
					// add infoTemplate (if present)
					$("." + imageTagger.options.infoClass).hide();
					if (imageTagger.options.infoTemplate != '') {
						imageTagger.options.data[imageTagger.options.isDrawing].el.append('<div class="' + imageTagger.options.infoClass + '" id="info_' + imageTagger.options.isDrawing + '" data-index="' + imageTagger.options.isDrawing + '">' + imageTagger.options.infoTemplate + '</div>');
						// prepopulate data if available
						if (typeof imageTagger.options.data[imageTagger.options.isDrawing].form_data != "undefined") {
							for(var i in imageTagger.options.data[imageTagger.options.isDrawing].form_data) {
								imageTagger.options.data[imageTagger.options.isDrawing].el.find("[name=" + i + "]").val(imageTagger.options.data[imageTagger.options.isDrawing].form_data[i]);
							}
							imageTagger.options.data[imageTagger.options.isDrawing].el.find("." + imageTagger.options.infoClass).hide();
						}
						// setup options
						imageTagger.options.data[imageTagger.options.isDrawing].el.find("." + imageTagger.options.infoClass)
							.css({"position":"absolute", "left":(tagCoords.bottomRight.x - tagCoords.topLeft.x) / 2 + 10, "top": Math.round((tagCoords.bottomRight.y - tagCoords.topLeft.y) / 2 * -1) })
							.mousedown(function(e) {
								e.stopPropagation();
							})
							.mouseup(function(e) {
								e.stopPropagation();
							})
							.find('.tagger-close').click(function(e) {
								e.stopPropagation();
								var $p = $(this).parents('.tagger-wrapper');
								$p.find('.' + imageTagger.options.infoClass).hide();
								$p.find(".tagger-tag").hide();
								$p.unbind('mousedown').unbind('mouseup');
							});
						imageTagger.options.data[imageTagger.options.isDrawing].el.find("input[type=submit]").click(function(e) {
							e.stopPropagation();
							var $parent = $(this).parents("." + imageTagger.options.infoClass);
							var index = parseInt($parent.data("index"));
							//$parent.find(".tagger-close").click();
							if ($.isFunction(imageTagger.options.save)) {
								// ! need to make sure this works properly - especially when coming back to save
								var data = {
									"form_data" : $parent.find("input, select, textarea, checkbox, radio").serializeArray(),
									"externalID" : imageTagger.options.data[index].externalID,
									"coordinates" : {
										"topLeft" : imageTagger.options.data[index].topLeft,
										"bottomRight" : imageTagger.options.data[index].bottomRight
									},
									"imageSize" : {
										"width" : imageTagger.width(),
										"height" : imageTagger.height()
									},
									"index" : index
								};
								imageTagger.options.save.call(this, data);
							}
						});
						/*
						imageTagger.options.data[imageTagger.options.isDrawing].el.find(".cancel").click(function(e) {
							//e.preventDefault();
							$(this).parents('.' + imageTagger.options.infoClass).hide();
							if ($.isFunction(imageTagger.options.cancel)) {
								//var serialized = $(this).parents("." + imageTagger.options.infoClass).find("input, select, textarea, checkbox, radio").serializeArray();
								imageTagger.options.cancel.call();
							}
						});
						*/
						imageTagger.options.data[imageTagger.options.isDrawing].el.find(".tagger-delete").click(function(e) {
							//e.preventDefault();
							if (confirm('Are you sure you want to delete this tag?')) {
								var $parent = $(this).parents("." + imageTagger.options.infoClass);
								var index = parseInt($parent.data("index"));
								if ($.isFunction(imageTagger.options.delete)) {
									//var serialized = $(this).parents("." + imageTagger.options.infoClass).find("input, select, textarea, checkbox, radio").serializeArray();
									imageTagger.options.delete.call(this, imageTagger.options.data[index].externalID);
								}
								imageTagger.options.data.splice(index, 1);
								$parent.parents('.tagger-wrapper').remove();
							}
						});
					}
					// remove drawing flag
					imageTagger.options.isDrawing = false;
					/*
					imageTagger.options.data[imageTagger.options.isDrawing].el.click(function() {
						if (imageTagger.options.infoTemplate != '') {
							$(this).find("." + imageTagger.options.infoClass).show();
						}
					});
					*/
				}).mousemove(function(e) {
					e.preventDefault();
					if (imageTagger.options.isDrawing !== false) {
						var $t = $(this);
						// change existing tag
						var coords = coordinates($t, e.clientX, e.clientY);
						var tagCoords = tagCoordinates($t, imageTagger.options.data[imageTagger.options.isDrawing].topLeft, coords);
						imageTagger.options.data[imageTagger.options.isDrawing].el.css({"top":tagCoords.topLeft.y, "left": tagCoords.topLeft.x}).width(tagCoords.bottomRight.x - tagCoords.topLeft.x).height(tagCoords.bottomRight.y - tagCoords.topLeft.y).
							find(".tagger-tag").width(tagCoords.bottomRight.x - tagCoords.topLeft.x).height(tagCoords.bottomRight.y - tagCoords.topLeft.y);
					}
				});
				
				
				
				// ! add tags if already set
				if (imageTagger.options.data.length > 0) {
					for(var i in imageTagger.options.data) {
						imageTagger.options.data[i].id = "tagger_" + i;
						// create element
						var $tag = $('<div class="tagger-wrapper" style="position:absolute"></div>').appendTo(imageTagger.wrapper).css({"top": (imageTagger.options.data[i].topLeft.y + imageTagger.options.data[i].bottomRight.y) / 2, "left": (imageTagger.options.data[i].topLeft.x + imageTagger.options.data[i].bottomRight.x) / 2}).width(0).height(0)
						//.click(function(e) {
						//	e.stopPropagation();
						//	$(this).find("." + imageTagger.options.infoClass).show();
						//})
						.mousedown(function(e) {
							e.stopPropagation();
						})
						.mouseup(function(e) {
							e.stopPropagation();
						});
						$tag.html('<span class="tagger-dot"></span><div class="tagger-tag" id="tagger_' + i + '"></div>');
						$tag.find('.tagger-tag').hide().css(imageTagger.options.tagCss); //.width(imageTagger.options.data[i].bottomRight.x - imageTagger.options.data[i].topLeft.x).height(imageTagger.options.data[i].bottomRight.y - imageTagger.options.data[i].topLeft.y).css({"left" : (imageTagger.options.data[i].bottomRight.x - imageTagger.options.data[i].topLeft.x) / 2 * -1, "top" : (imageTagger.options.data[i].bottomRight.y - imageTagger.options.data[i].topLeft.y) / 2 * -1});
						imageTagger.options.data[i].el = $tag;
						imageTagger.options.isDrawing = i;
						imageTagger.wrapper.mouseup();
					}
				}
					
				return this;
			},
			setExternalID: function(key, externalID) {
				//console.log(imageTagger.options);
				if (key !== false && key !== null && typeof imageTagger.options.data[key] != "undefined") {
					imageTagger.options.data[key].externalID = externalID;
				}
			},
			open: function(key) {
				if (key !== false && key !== null && typeof imageTagger.options.data[key] != "undefined") {
					imageTagger.options.data[key].el.click();
				}
			},
			close: function(key) {
				if (key !== false && key !== null && typeof imageTagger.options.data[key] != "undefined") {
					imageTagger.options.data[key].el.find(".tagger-close").click();
				}
			},
			delete: function(externalID) {
				var found = false;
				for (var i in imageTagger.options.data) {
					if (externalID == imageTagger.options.data[i].externalID || parseInt(imageTagger.options.data[i].externalID) == parseInt(externalID)) {
						found = i;
						/*
						var $parent = $(this).parents("." + imageTagger.options.infoClass);
						var index = parseInt($parent.data("index"));
						if ($.isFunction(imageTagger.options.cancel)) {
							//var serialized = $(this).parents("." + imageTagger.options.infoClass).find("input, select, textarea, checkbox, radio").serializeArray();
							imageTagger.options.delete.call(this, imageTagger.options.data[index].externalID);
						}
						imageTagger.options.data.splice(index, 1);
						$parent.parents('.tagger-wrapper').remove();
						*/
					}
				}
				if (found === false) {
					return false;
				}
				else {
					if (confirm('Are you sure you want to delete this tag?')) {
						if ($.isFunction(imageTagger.options.delete)) {
							//var serialized = $(this).parents("." + imageTagger.options.infoClass).find("input, select, textarea, checkbox, radio").serializeArray();
							imageTagger.options.delete.call(this, imageTagger.options.data[found].externalID);
						}
						imageTagger.options.data[found].el.remove();
						imageTagger.options.data.splice(found, 1);
						return true;
					}
					else {
						return false;
					}
				}
			}
		
		};
		
		
		///////////////// PRIVATE FUNCTIONS \\\\\\\\\\\\\\\\
		function coordinates($wrapper, x, y) {
			var offset = $wrapper.offset();
			return {
				x: Math.floor(x - offset.left + window.scrollLeft()),
				y: Math.floor(y - offset.top + window.scrollTop())
			};
		}
		
		function tagCoordinates($wrapper, position1, position2) {
			var topLeft = {
				x: 0,
				y: 0
			}, bottomRight = {
				x: 0,
				y: 0
			};
			var borderLeftWidth = parseInt($wrapper.find(".tagger-tag").eq(0).css("border-left-width"));
			var borderTopWidth = parseInt($wrapper.find(".tagger-tag").eq(0).css("border-top-width"));
			if (position1.x <= position2.x) {
				topLeft.x = position1.x - borderLeftWidth;
				bottomRight.x = position2.x - borderLeftWidth;
			}
			else {
				topLeft.x = position2.x - borderLeftWidth;
				bottomRight.x = position1.x - borderLeftWidth;
			}
			
			if (position1.y <= position2.y) {
				topLeft.y = position1.y - borderTopWidth;
				bottomRight.y = position2.y - borderTopWidth;
			}
			else {
				topLeft.y = position2.y - borderTopWidth;
				bottomRight.y = position1.y - borderTopWidth;
			}
			
			return {
				"topLeft" : topLeft,
				"bottomRight" : bottomRight
			}
		}
		
		///////////////// INIT STUFF \\\\\\\\\\\\\\\\
		
		if (methods[options]) {
			return methods[options].apply(this, Array.prototype.slice.call(arguments, 1));
		}
		else if (typeof options === 'object' || !options) {
			return methods.init.apply(this, arguments);
		}
	}
	
	// method-wide variables
	var imageTagger;

}(jQuery, jQuery(window)));